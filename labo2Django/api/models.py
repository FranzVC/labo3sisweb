from django.db import models

# Create your models here.
class Alumno(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField()
    email =  models.TextField()
    age = models.IntegerField()
    sex = models.TextField()
    matFav = models.TextField()

    def __str__(self):
        return self.name