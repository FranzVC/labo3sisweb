from tastypie.resources import ModelResource
from api.models import Alumno
from tastypie.authorization import Authorization

class AlumnoResource(ModelResource):
    class Meta:
        queryset = Alumno.objects.order_by('id')
        resource_name = 'alumno'
        authorization = Authorization()