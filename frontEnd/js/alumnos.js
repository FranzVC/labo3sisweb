function getAlumnos() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonObj = JSON.parse(this.responseText);
            populateTable(jsonObj.objects);
        }
    };
    xmlhttp.open("GET", "http://127.0.0.1:8000/api/alumno/", true);
    xmlhttp.send();
}

function populateTable(jsonObj) {
    var col = [];
    for (let index = 0; index < jsonObj.length; index++) {
        for (var key in jsonObj[index]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    var table = document.createElement("table");
    table.setAttribute('class', 'table table-striped');

    var thead = document.createElement("thead")
//-1
    for (var i = 0; i < col.length ; i++) {
        var th = document.createElement("th");
        th.innerHTML = col[i];
        th.setAttribute('class', 'scope');
        thead.appendChild(th);
    }

    var th = document.createElement("th");
    th.innerHTML = "Editar"
    th.setAttribute('class', 'scope');
    thead.appendChild(th);

    th = document.createElement("th");
    th.innerHTML = "Eliminar"
    th.setAttribute('class', 'scope');
    thead.appendChild(th);

    table.appendChild(thead)

    for (var i = 0; i < jsonObj.length; i++) {
        tr = table.insertRow(-1);
        var id;
        //-1
        for (var j = 0; j < col.length ; j++) {
            var cell = tr.insertCell(-1);
            cell.innerHTML = jsonObj[i][col[j]];
            if (j == 3) {
                id = jsonObj[i][col[j]];
            }
        }
        var cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="putAlumno('+ id +')" class="waves-effect waves-light btn black">Editar</button>'
        cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="deleteAlumno(' + id + ')" class="waves-effect waves-light btn black">Eliminar</button>'
    }
    document.getElementById("alumnos").appendChild(table);
}

function newAlumno() {
    window.location.href = '/frontEnd/createAlumno.html';
}