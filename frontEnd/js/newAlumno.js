function registerAlumno(){
    var _id = document.getElementById("alumno_id").value;
    var alumno_name = document.getElementById("alumno_name").value;
    var address =document.getElementById("address").value
    var email = document.getElementById("email").value;
    var age = document.getElementById("age").value;
    var sex = document.getElementById("sex").value
    var matFav = document.getElementById("matFav").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 201) {
            console.log(this.responseText);
            window.location.href='/frontEnd/showAlumno.html';
        }
    };
    xmlhttp.open("POST", "http://127.0.0.1:8000/api/alumno/", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify({
        "_id" : _id,
        "name" : alumno_name,
        "address" : address,
        "email" : email,
        "age" : age,
        "sex" : sex,
        "matFav" : matFav
    }))

}
